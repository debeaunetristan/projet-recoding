# Projet Recoding

## Static
**Static p5** https://editor.p5js.org/kau.grv/sketches/OC2bP--qy  
original version (brown & black, 6*6) fitting the original artwork.  
You can change the numbers of rows and columns in DrawMesh : DrawMesh(rows, columns)

## Dynamic
**Dynamic p5** https://editor.p5js.org/kau.grv/sketches/tFSqdts4U  
finale dynamic version. 
- Density follows the mouse
- Colors change on click

## Dynamic : alternative versions
**v1** https://editor.p5js.org/kau.grv/full/DPVIfKDXr 
mirrored mouse following 

**v2** https://editor.p5js.org/kau.grv/full/ad-vME9KU
inverted density on mouse following 

**v3** https://editor.p5js.org/kau.grv/full/voRszm91N
black background, luminosity and density follow mouse

**v4** https://editor.p5js.org/kau.grv/full/y_ZcmatDm
pulsating density, change colours on click




## Links

thanks to Mr Robillard  
main http://mobitool.free.fr/edu/ea/  
recoding http://mobitool.free.fr/edu/ea/recoding.html
